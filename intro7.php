<?php 
    //by php built in function
    $number = array(1,2,3,4);
    if(in_array(2, $number))
    {
    	echo "Got two";
    }
    

    //bye own function 
    function check_array($user_value,$array)
    {
    	foreach ($array as $key => $value) {
	    	if($value==$user_value)
	    	{
	    		return true;
	    	}
        }
        return false;
    }

    if(check_array(3,$number))
    {
    	echo "Got three";
    }
    
?>